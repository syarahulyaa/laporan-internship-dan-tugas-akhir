from keras.models import load_model

new_model = load_model("E:/Kode Program/features_extraction.model")

# In[]
loss, acc = new_model.evaluate(test_input, test_labels, batch_size=32)
print("Done!")
print("Loss: %.4f, accuracy: %.4f" % (loss, acc))
