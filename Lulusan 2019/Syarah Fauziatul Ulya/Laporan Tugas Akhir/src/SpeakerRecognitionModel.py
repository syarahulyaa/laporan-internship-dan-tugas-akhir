# In[1]: Import Library / Modul
import librosa
import librosa.feature
import librosa.display
import glob
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.utils.np_utils import to_categorical

# In[2]: Fungsi display_mfcc
def display_mfcc(audio):
    y, _ = librosa.load(audio)
    mfcc = librosa.feature.mfcc(y) 
    
    plt.figure(figsize=(10, 4))
    librosa.display.specshow(mfcc, x_axis='time', y_axis='mel')
    plt.colorbar()
    plt.title(audio)
    plt.tight_layout()
    plt.show()
    
# In[3]: Memanggil fungsi display_mfcc
display_mfcc('E:/Kode Program/Dataset/speaker/19/19-198-0001.au')

# In[4]: Memanggil fungsi display_mfcc
display_mfcc('E:/Kode Program/Dataset/speaker/26/26-495-0000.au')

# In[5]: Memanggil fungsi display_mfcc
display_mfcc('E:/Kode Program/Dataset/speaker/27/27-123349-0000.au')

# In[6]: Fungsi extract_features_audio
def extract_features_audio(f):
    y, _ = librosa.load(f) 
    mfcc = librosa.feature.mfcc(y)    
    mfcc /= np.amax(np.absolute(mfcc))
    
    return np.ndarray.flatten(mfcc)[:25000] 

# In[7]:
def generate_features_and_labels():
    all_features = [] 
    all_labels = []
    
    speakers= ['19', '26', '27', '32', '39', '40', '60', '78', '83', '87', '89', '103', '118'] 
    for speaker in speakers: 
        
        sound_files = glob.glob('E:/Kode Program/Dataset/speaker/'+speaker+'/*.au')
        print('Processing %d audio in %s speaker...' % (len(sound_files), speaker))
        for f in sound_files:
            
            features = extract_features_audio(f)
            all_features.append(features)
            all_labels.append(speaker)
        
    label_uniq_ids, label_row_ids = np.unique(all_labels, return_inverse=True)
    label_row_ids = label_row_ids.astype(np.int32, copy=False)
    onehot_labels = to_categorical(label_row_ids, len(label_uniq_ids))

    return np.stack(all_features), onehot_labels

# In[8]: Passing parameter dari fitur ekstraksi menggunakan MFCC
features, labels = generate_features_and_labels()

# In[9]: Menampilkan jumlah bentuk dari features dan labels
print(np.shape(features))
print(np.shape(labels))

# In[10]:
training_split = 0.8

# In[11]:
alldata = np.column_stack((features, labels))

# In[12]:
np.random.shuffle(alldata)
splitidx = int(len(alldata) * training_split)
train, test = alldata[:splitidx,:], alldata[splitidx:,:]

# In[13]:
print(np.shape(train))
print(np.shape(test))

# In[14]:
train_input = train[:,:-13] 
train_labels = train[:,-13:]

# In[15]:
test_input = test[:,:-13]
test_labels = test[:,-13:]

# In[16]:
print(np.shape(train_input))
print(np.shape(train_labels))

# In[17]: membuat model sequential dari keras
model = Sequential([
        Dense(85, input_dim=np.shape(train_input)[1]),
        Activation('relu'),
        Dense(13),
        Activation('softmax'), 
        ])

# In[18]:
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

print(model.summary())

# In[19]: Pelatihan dari dari train
model.fit(train_input, train_labels, epochs=13, batch_size=32, validation_split=0.2) #cek nilai skor cross validation

# In[20]:
loss, acc = model.evaluate(test_input, test_labels, batch_size=32)
print("Done!")
print("Loss: %.4f, accuracy: %.4f" % (loss, acc))

# In[20]: save model
model.save("E:/Kode Program/features_extraction.model")

# In[21]: Predict
model.predict(test_input[:1])
