# In[1]
from keras.models import load_model
import numpy as np
import librosa
import glob
from keras.utils.np_utils import to_categorical

# In[2]
new_model = load_model("E:/Kode Program/features_extraction.model")

def extract_feature_audio(fx):
    #membuat fungsi dengan inputan f
    y, _ = librosa.load(fx) 
    #variabel y untuk meload inputan f
    
    # get Mel-frequency cepstral coefficients
    mfcc = librosa.feature.mfcc(y)
    #variabel mfcc untuk membuat feature dari variabel y
    
    # normalize values between -1,1 (divide by max)
    mfcc /= np.amax(np.absolute(mfcc))
    #membuat normalisasi nilai antara -1 dan 1
    
    return np.ndarray.flatten(mfcc)[:25000] 
    #diambil 25000 row pertama berdasarkan durasi suara

# In[3]
def generate_feature_and_label():
    all_label = []
    all_feature = []
    speak = ['test']
    #variabel speaker berisi nama-nama folder dari dataset yang digunakan
    for sp in speak: 
        sound_file = glob.glob('E:/Kode Program/Dataset/'+sp+'/*.au')
        #membuat atribut sound_files yang berisi perintah looping per folder
        print('Processing %d audio ...' % len(sound_file), sp)
        #menampilkan jumlah audio yang dieksekusi
        for fx in sound_file:
        #membuat perintah fungsi dari sound_files
            
            feature = extract_feature_audio(fx)
            #variabel features untuk memanggil fungsi extract_feature_audio, f sebagai inputan didalam soundfile
            all_feature.append(feature)
            #vektoriksasi, memasukkan semua fitur dengan perintah append ke dalam all_features
            all_label.append(sp)
            #append seperti stack=ditumpuk
        
    # convert labels to one-hot encoding
    label_uniq_id, label_row_id = np.unique(all_label, return_inverse=True)
    label_row_id = label_row_id.astype(np.int32, copy=False)
    onehot_label = to_categorical(label_row_id, len(label_uniq_id))
    return np.stack(all_feature), onehot_label
# In[4]: passing parameter dari fitur ekstraksi menggunakan MFCC
feature, label = generate_feature_and_label()

# In[5]
new_model.predict(feature[:3])
