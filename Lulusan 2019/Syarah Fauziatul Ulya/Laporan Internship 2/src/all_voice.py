# In[1]:
import librosa #membaca lagu
import librosa.feature
import librosa.display
import glob #mengambil data audio
import numpy as np #array
import matplotlib.pyplot as plt #membuat plot
from keras.models import Sequential #keras ~ tensorflow
from keras.layers import Dense, Activation
from keras.utils.np_utils import to_categorical
# In[2]:
def display_mfcc(song):
    y, _ = librosa.load(song)
    mfcc = librosa.feature.mfcc(y) #mfcc itu vektor
    
    plt.figure(figsize=(10, 4))
    librosa.display.specshow(mfcc, x_axis='time', y_axis='mel')
    plt.colorbar()
    plt.title(song)
    plt.tight_layout()
    plt.show()
# In[3]: Memanggil fungsi display_mfcc
display_mfcc('E:/ubtShare/Repositories/MyProject/ceria/Syarah-Fauziatul-Ulya/Internship/Kode Program/speaker/syarahfu/syarahfu.000.au')
# In[4]:
display_mfcc('E:/ubtShare/Repositories/MyProject/ceria/Syarah-Fauziatul-Ulya/Internship/Kode Program/speaker/dinidml/dinidml.000.au')
# In[5]:
display_mfcc('E:/ubtShare/Repositories/MyProject/ceria/Syarah-Fauziatul-Ulya/Internship/Kode Program/speaker/hanifahsa/hanifahsa.000.au')
# In[6]:
def extract_features_song(f):
    y, _ = librosa.load(f) #meload f
    
    # get Mel-frequency cepstral coefficients
    mfcc = librosa.feature.mfcc(y)
    # normalize values between -1,1 (divide by max)
    mfcc /= np.amax(np.absolute(mfcc))
    
    return np.ndarray.flatten(mfcc)[:25000] #diambil 25000 row pertama dari vektorisasi MFCC
# In[7]:
def generate_features_and_labels():
    all_features = []
    all_labels = []
    
    speakers= ['syarahfu', 'dinidml', 'hanifahsa'] #sesuai nama folder
    for speaker in speakers: #looping dari folder speakers
        sound_files = glob.glob('E:/ubtShare/Repositories/MyProject/ceria/Syarah-Fauziatul-Ulya/Internship/Kode Program/speaker/'+speaker+'/*.au')
        print('Processing %d audio in %s speakers...' % (len(sound_files), speaker))
        for f in sound_files: #fsebagai inputan didalam soundfile
            features = extract_features_song(f) #vektoriksasi
            all_features.append(features) #append seperti stack=ditumpuk
            all_labels.append(speaker)
        
    # convert labels to one-hot encoding
    label_uniq_ids, label_row_ids = np.unique(all_labels, return_inverse=True)
    label_row_ids = label_row_ids.astype(np.int32, copy=False)
    onehot_labels = to_categorical(label_row_ids, len(label_uniq_ids))
    return np.stack(all_features), onehot_labels
# In[8]: passing parameter dari fitur ekstraksi menggunakan MFCC
features, labels = generate_features_and_labels()
# In[9]:
print(np.shape(features))
print(np.shape(labels))
# In[10]:
training_split = 0.8
# In[11]:
# last column has speak, turn it into unique ids
alldata = np.column_stack((features, labels))
# In[12]:
np.random.shuffle(alldata)
splitidx = int(len(alldata) * training_split)
train, test = alldata[:splitidx,:], alldata[splitidx:,:]
# In[13]:
print(np.shape(train))
print(np.shape(test))
# In[14]:
train_input = train[:,:-3] #kolom 3 terakhir tidak diikutsertakan
train_labels = train[:,-3:]
# In[15]:
test_input = test[:,:-3]
test_labels = test[:,-3:]
# In[16]:
print(np.shape(train_input))
print(np.shape(train_labels))
# In[17]: membuat model sequential dari keras
model = Sequential([
        Dense(100, input_dim=np.shape(train_input)[1]),
        Activation('relu'), #relu=rectifier linier unit, fungsinya untuk mencari nilai maksimum yang akan dipilih
        Dense(3),
        Activation('softmax'), 
        ])
# In[18]:
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
print(model.summary()) #munculin summary
# In[19]: pelatihan dari dari training
model.fit(train_input, train_labels, epochs=10, batch_size=32, validation_split=0.2) #cek nilai skor cross validation
# In[20]:
loss, acc = model.evaluate(test_input, test_labels, batch_size=32)
# In[21]:
print("Done!")
print("Loss: %.4f, accuracy: %.4f" % (loss, acc))
# In[22]:
model.predict(test_input[:1]) #memasukkan 1 row data(suara) dari test input